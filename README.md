[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)


## requirements

* nodejs 6.9+

for docker functionality:

* docker hub account
* docker


### dev workstation installation

* clone repository to local file system
* change directory to project root
* open bash shell and execute ``npm install`` to install all the required dependencies



## commands 
The tool is intended to run from the commandline and supports the following commands:


### server
does stuff and things while also exposing a useful REST api

```bash
# do stuff and things and listen on default port 8080
$ npm start -- server

```

for a full list of available parameters and their meanings

```bash
$ npm start -- server --help
```


## docker support

the service is designed to be packaged and run within a docker container. see the *Dockerfile* in this repository. 
we use bitbucket's pipelines to automatically build, package (into a docker image) and publish to docker hub.

the service may be run _locally_ in a docker container via a `docker run` command such as the following:

```bash
$ docker run --rm \
        -v `pwd`:/project \
        -w /project \
        -p 8080:8080 \
        resin/raspberry-pi-alpine-node:6-slim \
        npm start -- server
```

### docker hub
the built service docker image is published to a docker hub repository that you specify.
in order for the publication to function, the following 'bitbuck pipeline environment variables' (set per repository) must be created and populated.

* DOCKER_USERNAME: your dockerhub account username 
* DOCKER_PASSWORD: your dockerhub account password

the destination docker repository is specified in _package.json_'s configuration section. 
```json
...
    "config": {
        ...
        "dockerRepository": "yourusername/mousetrap"
    },
...
```
NB: when cloning this repository, be sure to set this value to *your* docker hub repository.


## development workflow

### commit changes ( structured )
You fix bugs and add features by altering, creating and deleting files in
the repository.

commits are **not** done via the normal 'git commit' command. instead, we
make use of structured commits that ensure our commit messages are consistently formated.
we do this by invoking `npm run commit` - this will cause a series of prompts gathering various key bit of information.

### release a version
Periodically we will officially release new versions of the application.
before publishing the package, we will run `npm run release` and then perform a a standard git commit ( follow the instructions displayed after `npm run release` completes to ensure the commit command is invoked with the correct parameters)

The release script will:

1. automatically update the CHANGELOG.md file
2. update the package.json version number
3. tag the repository with the version number.