const _               = require("lodash")
const Server          = require("./server")

require("yargs")
    .usage('npm start -- <cmd> [args]')
    
    .command(
      Server.usage(), 
      Server.description(), 
      Server.args(),
      (argv) =>  Server.fromArgs(argv).run() 
    )

    .help()
    .argv