const _               = require("lodash")
const express         = require("express")
const bodyParser      = require("body-parser")
const require_tree    = require("require-tree")
const Haikunator      = require("haikunator")

class Server {

  constructor(port){
    
    this.config = {
      port: port,
      log: console,
      instanceId: new Haikunator().haikunate()
    }

    this.httpServer      = express()
    this.httpServer.use(bodyParser.json())

    require_tree("./api", { filter: /^v1/, each: (module) => module(this.httpServer, this.config) })

    //must be last middleware assigned
    require("./api/errorHandler.js")
  }

  run(){
    this.httpServer.listen(this.config.port, () => {
      this.config.log.info(`
        server listening on port ${this.config.port}
        
        instanceId:     ${this.config.instanceId}

        product:        ${process.env.npm_package_name}
        version:        ${process.env.npm_package_version}
        node version:   ${process.env.NODE_VERSION || "not-specified"}

        commit:         ${process.env.npm_package_gitHead}
        `)

    })
  }

  static fromArgs(args) {
    return new Server(args.port)
  }

  static usage(){ return "server [port]" }

  static description(){ return "serves stuff and things" }

  static args(){

    return { 
      port: {
        default: 8080,
        type: "number",
        describe: "port the service will listen to"
      }
    }
  }

}

module.exports = Server
