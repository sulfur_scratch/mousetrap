module.exports = function defaultModule(httpServer, config) {

  config.log.info(`configuring default api`)

  function getDefault(ip, instanceId){
    return { 
      service: process.env.npm_package_name,
      version: process.env.npm_package_version,
      instanceId: instanceId,
      requestedBy: ip || "unknown"
    }
  }
  
  // publish REST api
  httpServer.get("/", (req, res)=> {
    let result = getDefault(req.ip || req.ips, config.instanceId)
    res
      .status(200)
      .json(result)
  })

  //publish module API
  return {
    getDefault: getDefault
  }
}