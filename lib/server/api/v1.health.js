module.exports = function healthModule(httpServer, config) {
  const baseUri = "/v1/health"
  let currentHealth = null

  config.log.info(`configuring health api ${baseUri}`)

  function getHealth(){
    return currentHealth
  }

  function setHealth(statusCode, errorMessage = ''){
  
    var result = {
      status: "up",
      code: statusCode,
      version: process.env.npm_package_version,
      instanceId: config.instanceId
    }
    if (statusCode != 200) {
      result.status = "down"
      result.errors = [errorMessage || "something went wrong..."]
    }
    return result
  }

  currentHealth = setHealth(200)

  // publish REST api
  httpServer.get(`${baseUri}`, (req, res)=> {
      let result = getHealth()
     
      res
        .status(result.code)
        .json(result)
    })

  // publish REST api
  httpServer.put(`${baseUri}/:code/:message?`, (req, res)=> {
      config.log.info("updating health status", req.params.code)
      currentHealth = setHealth(req.params.code, req.params.message)
      res
        .status(200)
        .json(currentHealth)
    })

  // publish module api
  return {
    getHealth: getHealth,
    setHealth: setHealth
  }
}