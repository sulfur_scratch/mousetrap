module.exports = (httpServer) => {
  httpServer.use((err, req, res, next)=> {
    try{
      console.error(`error encountered processing request for '${req.originalUrl}'`, err)
    }
    finally {
      res
        .status(500)
        .json({ message: "error encountered processing request."})
    }
  })
}