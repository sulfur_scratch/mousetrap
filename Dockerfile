FROM resin/raspberry-pi-alpine-node:6-slim

LABEL environment 1.1
LABEL company acme corp ltd.

ENV INITSYSTEM=off PACKAGE_PATH=/opt/project

EXPOSE 8080

# errors out when run outside of raspberry pi :(
# RUN apt update && apk --no-cache add curl

ADD . $PACKAGE_PATH
WORKDIR $PACKAGE_PATH

ENTRYPOINT ["npm", "start", "--"]
CMD ["server"]


