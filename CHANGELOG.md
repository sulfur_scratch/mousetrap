# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.2.4"></a>
## [1.2.4](https://bitbucket.org/sulfur_scratch/mousetrap/compare/v1.2.3...v1.2.4) (2017-10-20)



<a name="1.2.3"></a>
## [1.2.3](https://bitbucket.org/sulfur_scratch/mousetrap/compare/v1.2.2...v1.2.3) (2017-10-20)



<a name="1.2.2"></a>
## [1.2.2](https://bitbucket.org/sulfur_scratch/mousetrap/compare/v1.2.1...v1.2.2) (2017-10-20)



<a name="1.2.1"></a>
## [1.2.1](https://bitbucket.org/sulfur_scratch/mousetrap/compare/v1.2.0...v1.2.1) (2017-10-20)



<a name="1.2.0"></a>
# [1.2.0](https://bitbucket.org/sulfur_scratch/mousetrap/compare/v1.1.0...v1.2.0) (2017-10-20)


### Features

* **server:** add more detailed startup information ([0e5eac7](https://bitbucket.org/sulfur_scratch/mousetrap/commits/0e5eac7))



<a name="1.1.0"></a>
# 1.1.0 (2017-10-19)


### Features

* **server:** provide basic server functionality ([78eda6f](https://bitbucket.org/sulfur_scratch/mousetrap/commits/78eda6f))



<a name="1.2.0"></a>
# 1.2.0 (2017-10-19)


### Features

* **server:** provide basic shell server ([ecf7755](https://bitbucket.org/sulfur_scratch/mousetrap/commits/ecf7755))



<a name="1.1.0"></a>
# 1.1.0 (2017-10-19)


### Features

* **server:** provide basic shell service implementation ([6658291](https://bitbucket.org/sulfur_scratch/mousetrap/commits/6658291))
