#!/usr/bin/env bash

# ***************************************************************************************************
# Script is used to build, test, package and publish the component within a docker container
#
# $ docker run \
#         --rm \
#         -e DOCKER_USERNAME=myUserName
#         -e DOCKER_PASSWORD=myPassword
#         -v `pwd`:/project \
#         --workdir /project \
#         node:6 \
#         chmod +x ci.sh && ./ci.sh %build.counter% %run-unit-tests% %run-metrics% %run-release% %publish-package%
#
# ***************************************************************************************************

set -e

if [[ $1 =~ '^[0-9]+$' ]]; then
    echo $0: usage: ci.sh build_number [unit_test] [metrics] [release] [publish] [commit]
    exit 1
fi


BUILD_NUMBER=$1
UNIT_TEST=${2:-true}
METRICS=${3:-false}
RELEASE=${4:-false}
PUBLISH=${5:-false}
COMMIT=${6:-unspecified}

PACKAGE_VERSION=$(sed -n 's/"version": "\([^"]*\)",/\1/p' package.json | tr -d '[[:space:]]')
PACKAGE_NAME=$(sed -n 's/"name": "\([^"]*\)",/\1/p' package.json | tr -d '[[:space:]]')
REPOSITORY=$(sed -n 's/"dockerRepository": "\([^"]*\)"/\1/p' package.json | tr -d '[[:space:]]')

if [ -z $PACKAGE_VERSION ]; then echo "Failed to find package version in package.json"; exit 1; fi
if [ -z $PACKAGE_NAME ]; then echo "Failed to find package name in package.json"; exit 1; fi
if [ -z $REPOSITORY ]; then echo "Failed to find artifactory url in package.json"; exit 1; fi

if [ "$RELEASE" != true ]; then
  PACKAGE_VERSION=$(echo $PACKAGE_VERSION | sed "s/\.[0-9]*$/&-$(printf "%04d" $BUILD_NUMBER)/g")
fi

echo "**************************************************"
echo "package.name:       $PACKAGE_NAME"
echo "package.version:    $PACKAGE_VERSION"
echo "package.unit-test:  $UNIT_TEST"
echo "package.metrics:    $METRICS"
echo "package.release:    $RELEASE"
echo "package.publish:    $PUBLISH"
echo "**************************************************"

function clean(){
  echo "cleaning build environment..."
  rm rm -fr ./coverage \
    && rm -fr ./.nyc_output \
    && rm -f xunit.xml
}

clean

npm install \
  && chmod -R 777 node_modules


if [ "$UNIT_TEST" = true ]; then
  echo "running tests..."
  npm run test
else
  echo "skipping unit tests..."
fi

if [ "$METRICS" = true ]; then
  echo "running netrics..."
  npm run netrics

  # TODO implement/run sonar netrics reporting
fi


if [ "$RELEASE" = true ]; then

  echo "building for release..."

  echo "packaging for release..."
  npm prune --production \
    && clean

else
  echo "skipping release steps..."
fi


if [ "$PUBLISH" = true ]; then
    echo "publishing to repository ($REPOSITORY)..."

    docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD
    docker build \
            --label name=$PACKAGE_NAMEN \
            --label version=$PACKAGE_VERSION \
            --label build=$BUILD_NUMBER \
            --label commit=$COMMIT \
            -t $REPOSITORY:$PACKAGE_VERSION \
            .
    docker push $REPOSITORY:$PACKAGE_VERSION

fi