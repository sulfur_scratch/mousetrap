
``GET ``
default route - gets basic service instance information


``GET v1/health``
gets the current health status of the service instance
response with either 200 for up or +400 for unhealth

``PUT /v1/health/:code/:message?``
sets the health of the service to a desired state. this method is primarily for testing various cluster/system hosting features and failure modes.


